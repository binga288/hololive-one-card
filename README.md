# Hololive 一卡通雞雞人
## 事前準備
### 登入資訊
到 config.ini 設定你的資訊
```ini
[DEFAULT]
login_email = 你的帳號
```
### 購買資訊
到 config.ini 設定你的資訊
```ini
[CART]
cart_surname = 姓
cart_name = 名子
cart_city = 城市
cart_address = 地址
cart_zip = 郵遞區號
cart_phone = 購買人電話
```
### 購買物品
```ini
[BUY]
; buy_card: 指定購買商品 suisei、ina、peko、all
buy_card = suisei
buy_number = 1
```
### Cookie
config.ini 中的 cart_cookie 需要自行取得
1. 先到 [推推官網](https://tuetuelook.com/) 登入
2. 登入後打開瀏覽器開發者工具(F12)，點選 Application(應用程式)
![Alt text](chrome_G5MOYAiRWb.png)
1. 在Strorage(儲存)中點選 Cookies，找到 tuetuelook.com 點選後找到 cart 點選後，複製Value
![Alt text](msedge_g9uHiHribW.png)
1. 將 cart 的內容貼到 config.ini 中的 cart_cookie
```ini
[CART]
cart_cookie = ...
```

## 注意事項
1. 登入帳號購物車務必為空，你會買到其他東西

## 使用方法
1. 執行 hololive.exe
2. 等待登入完畢
3. 等待輸入購買資訊
4. 手動完成剩餘步驟
