import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from tkinter import messagebox
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import requests
from selenium.webdriver.chrome.service import Service as ChromeService
import config as cfg


def AddCardToCart(id, cart_cookie):
    cookies_string = "cart=" + cart_cookie + ";"

    # 送出 Post 請求
    url = "https://tuetuelook.com/cart/add"

    payload = {'quantity': '1',
               'form_type': 'product',
               'utf8': '✓',
               'id': id
               }

    files = []
    headers = {
        'Cookie': cookies_string,
        'Content-Type': 'multipart/form-data',
    }
    print(cookies_string)

    response = requests.request(
        "POST", url, headers=headers, data=payload, files=files)
    print(response.status_code)


def cookie_present(cookie_name):
    # 這個函數將檢查是否存在特定的cookie
    cookies_list = driver.get_cookies()
    for cookie in cookies_list:
        if (cookie['name'] == cookie_name):
            return True
    return False


if __name__ == '__main__':
    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')

    # 創建一個新的Chrome瀏覽器實例
    driver = webdriver.Chrome("./chromedriver", options=options)

    # 導航到選課頁面
    driver.get("https://shopify.com/78342422809/account?locale=zh-TW")

    try:
        WebDriverWait(driver, 60).until(EC.element_to_be_clickable(
            (By.CSS_SELECTOR, "#account_lookup > div:nth-child(13) > button")))

        email_field = driver.find_element(By.CSS_SELECTOR, "#account_email")
        email_field.send_keys(cfg.login_email)
        login_btn = driver.find_element(
            By.CSS_SELECTOR, "#account_lookup > div:nth-child(13) > button")
        login_btn.click()
    except:
        print("無法登入")
        driver.quit()

    try:
        # 等待出現"訂單"H1
        WebDriverWait(driver, 60).until(EC.presence_of_element_located(
            (By.CSS_SELECTOR, "#accessible-heading")))
        driver.get("https://tuetuelook.com/cart")
        driver.add_cookie(
            {'name': 'localization', 'value': 'TW', 'domain': 'tuetuelook.com'})
    except:
        print("無法確認登入")
        driver.quit()

    WebDriverWait(driver, 60).until(EC.presence_of_element_located(
        (By.CSS_SELECTOR, "#shopify-section-sections--19778770108697__header > sticky-header > header > a > div > img")))
    WebDriverWait(driver, timeout=60).until(lambda d: cookie_present('cart'))
    cart_cookie = driver.get_cookie('cart')['value']

    print(cart_cookie)
    for i in range(0, int(cfg.buy_number)):
        if cfg.buy_card == 'suisei':
            AddCardToCart(cfg.card_suisei, cart_cookie)
        if cfg.buy_card == 'peko':
            AddCardToCart(cfg.card_peko, cart_cookie)
        if cfg.buy_card == 'ina':
            AddCardToCart(cfg.card_ina, cart_cookie)
        if cfg.buy_card == 'all':
            AddCardToCart(cfg.card_suisei, cart_cookie)
            AddCardToCart(cfg.card_peko, cart_cookie)
            AddCardToCart(cfg.card_ina, cart_cookie)

    while True:
        # cart_url = "https://tuetuelook.com/checkouts/cn/"+cart_cookie
        cart_url = "https://tuetuelook.com/checkout/"
        driver.get(cart_url)
        print(cart_url)
        try:
            WebDriverWait(driver, 20).until(EC.element_to_be_clickable(
                (By.CSS_SELECTOR, "#Form0 > div:nth-child(1) > div > div.VheJw > div.oQEAZ.WD4IV > div:nth-child(1) > button")))
        except:
            print("還沒開放")
            driver.get("https://tuetuelook.com/")
            conti = input("輸入q離開，其他繼續")
            if conti == 'q':
                break
            continue

        # 輸入運送資料
        driver.find_element(
            By.CSS_SELECTOR, "#TextField0").send_keys(cfg.cart_surname)
        driver.find_element(
            By.CSS_SELECTOR, "#TextField1").send_keys(cfg.cart_name)
        driver.find_element(
            By.CSS_SELECTOR, "#TextField2").send_keys(cfg.cart_address)
        driver.find_element(
            By.CSS_SELECTOR, "#TextField4").send_keys(cfg.cart_city)
        driver.find_element(
            By.CSS_SELECTOR, "#TextField5").send_keys(cfg.cart_zip)
        driver.find_element(
            By.CSS_SELECTOR, "#TextField6").send_keys(cfg.cart_phone)

        driver.find_element(
            By.CSS_SELECTOR, "#Form0 > div:nth-child(1) > div > div.VheJw > div.oQEAZ.WD4IV > div:nth-child(1) > button").click()
        messagebox.showinfo(title="輸入", message="完成剩餘輸入")
