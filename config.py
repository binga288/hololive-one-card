import configparser

# 45660247785753 Suisei
# 45660247752985 Peko
# 45660247818521 Ina

# 從config.ini中讀取cart cookie
config = configparser.RawConfigParser()
with open('config.ini', 'r', encoding='utf-8') as f:
    config.read_file(f)

login_email = config['DEFAULT']['login_email']

# 讀取購買資訊
cart_surname = config['CART']['cart_surname']
cart_name = config['CART']['cart_name']
cart_address = config['CART']['cart_address']
cart_city = config['CART']['cart_city']
cart_zip = config['CART']['cart_zip']
cart_phone = config['CART']['cart_phone']

card_suisei = config['CARD']['suisei']
card_peko = config['CARD']['peko']
card_ina = config['CARD']['ina']

buy_card = config['BUY']['buy_card']
buy_number = config['BUY']['buy_number']